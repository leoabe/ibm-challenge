# IBM Challenge

GIT repository for IBM Challenge Exercise 

## Usage

### Prerequisites

* Gradle 5.6.x or 6.x.x
* Java 8+ 

## Getting Started

This project has the function to build and return a sorted list of movie titles based on a string that is passed by query parameter

### Swagger 2
    
Access swagger-ui endpoint with:

```bash
http://localhost:8080/swagger-ui.html
```

### Command Line
    
Use the following syntax:

```bash
curl -X GET "http://localhost:8080/api/v1/movies?substr=substrValue" -H "accept: */*"
```
replace "substrValue" with what you desire

### Postman

Use the following url:

```bash
http://localhost:8080/api/v1/movies
```
and fill with query parameter 

```bash
substr
```
Endpoint Example: 
```bash
http://localhost:8080/api/v1/movies?substr=spiderman
```

### Result 

As result, the api will return a sorted list with movie titles, as an example using the key "spiderman"

```
[
  "Amazing Spiderman Syndrome",
  "Fighting, Flying and Driving: The Stunts of Spiderman 3",
  "Hollywood's Master Storytellers: Spiderman Live",
  "Italian Spiderman",
  "Spiderman",
  "Spiderman",
  "Spiderman 5",
  "Spiderman and Grandma",
  "Spiderman in Cannes",
  "Superman, Spiderman or Batman",
  "The Amazing Spiderman T4 Premiere Special",
  "The Death of Spiderman",
  "They Call Me Spiderman"
]
```

If the key that you used does not have any movie title related with, this response will be shown 

```
[
  "This sub string hasn't any titles associated with."
]
```