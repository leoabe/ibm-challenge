package com.ibm.challenge.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MovieSearchDTO {

	@JsonProperty("total_pages")
	int totalPages;

	private List<HashMap<Object, Object>> data = new ArrayList<>();
}
