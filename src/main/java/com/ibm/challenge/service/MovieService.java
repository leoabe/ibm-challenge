package com.ibm.challenge.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.ibm.challenge.controller.MovieController;
import com.ibm.challenge.dto.MovieSearchDTO;

@Service
public class MovieService {

	private static final Logger logger = LogManager.getLogger(MovieController.class);

	@Autowired
	private RestTemplate restTemplate;

	@Value("${movie.search.endpoint}")
	private String searchEndpoint;

	/**
	 * Search for all movies, format and return sorted list of their titles
	 * 
	 * @param -> subsrt
	 * @return -> titles
	 */
	public List<String> getMovieTitles(String subsrt) {
		List<String> titles = new ArrayList<String>();

		logger.info("Starting to mount sorted titles list");
		mountTitlesArray(subsrt, titles);

		return titles;
	}

	private List<String> mountTitlesArray(String subsrt, List<String> titles) {
		int totalPages;

		logger.info("Getting total pages");
		totalPages = getTotalPages(subsrt, titles);

		// Data from page 1 has already been inserted
		if (totalPages != 1) {
			for (int i = 2; i <= totalPages; i++) {
				MovieSearchDTO movieSearchData = getMoviesData(subsrt, i);
				addTitles(titles, movieSearchData.getData());
			}
		}

		titles.sort(String::compareToIgnoreCase);

		logger.info("Returning titles list");
		return titles;
	}

	private int getTotalPages(String subsrt, List<String> titles) {
		MovieSearchDTO movieSearchData = getMoviesData(subsrt, 1);

		// Getting movie titles data from get total pages request
		addTitles(titles, movieSearchData.getData());

		return movieSearchData.getTotalPages();
	}

	/**
	 * Every response return page 1 even though it has no data
	 * 
	 * @param subsrt
	 * @param totalPages
	 * @return
	 */
	private MovieSearchDTO getMoviesData(String subsrt, int page) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(searchEndpoint).queryParam("Title", subsrt)
				.queryParam("page", page);

		logger.info("Sending request for page: " + page);
		return restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, MovieSearchDTO.class).getBody();
	}

	private void addTitles(List<String> titles, List<HashMap<Object, Object>> movies) {
		if (movies.isEmpty()) {
			titles.add("This sub string hasn't any titles associated with.");
		} else {
			movies.forEach(movie -> {
				titles.add((String) movie.get("Title"));
			});
		}
	}

}
