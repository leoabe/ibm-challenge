package com.ibm.challenge.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.challenge.service.MovieService;

@RestController
@RequestMapping("/api/v1/movies")
@CrossOrigin(origins = "*")
public class MovieController {

	private static final Logger logger = LogManager.getLogger(MovieController.class);

	@Autowired
	private MovieService service;

	/**
	 * Build a sorted list of titles related to subsrt param
	 * 
	 * @param subsrt
	 * @return titles
	 */
	@GetMapping
	public List<String> getMovieTitles(@RequestParam("substr") String subsrt) {
		logger.info("Initializing get movie titles request");
		return service.getMovieTitles(subsrt);
	}
}
